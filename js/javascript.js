/**
 * @file
 * Javascript for ninesixtyrobots theme.
 */
(function ($) {

// Prefill the search box with Search... text.
Drupal.behaviors.ninesixtyrobots = {
  attach: function (context) {
    $('#edit-combine-wrapper input:text', context).autofill({
      value: "DPI, aplicación ....."
    });
  }
};

})(jQuery);
